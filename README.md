# CA Quiz Wordpress Plugin #

This plugin allows you to create & manage simple quizzes with multiple questions, post them at your blog's pages & send results to the user's email or produce a PDF containing this results.

### About CA Quiz ###

Originally this plugin was created for one of my clients as addition to a theme used on his site. Then it was transformed into a full scale plugin.

Version 1.0.0

It allows you to:

- Create, edit & delete tests
- Create edit & delete question for each test
- Manage answers for each question
- Shortcode to output the quiz in the post
- Ability to form PDF with test results
- Ability to send test results via E-mail
- Simple search functionality to allow comfortable manipulation with dozens of quizzes

### Installing plugin ###

1. Download source code
2. Upload it into plugin directory of your Wordpress website
3. Activate the plugin in the admin area
4. Optionally you can pack the source code into *.zip archive & use admin interface to upload the plugin via browser.

### Contacts ###

You can contact me using my website at [en.codeartist.ru](http://www.en.codeartist.ru)