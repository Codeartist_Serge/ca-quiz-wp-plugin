angular.
module('ca_quiz').
filter('htmloutput', ['$sce', function($sce) {
	return function(text) {
		return $sce.trustAsHtml(text);
	};
}]);