angular.
module('ca_quiz').
component('caQuizFront', {
	templateUrl: '/wp-content/plugins/ca_quizes/layouts/angular_templates/ca_quiz_shortcode.html',
	//template: '<p>Angular is here!</p>',
	controller: function ca_quiz_ctrl($http, $httpParamSerializerJQLike, $attrs) {
		_this = this;
		_this.test_title = null; // Test title
		_this.maxid = null; // Current question angular id
		_this.preloader = true; // Preloader state
		_this.intro = true; // Intro form state
		_this.final = false; // Final score screen state
		_this.email_sent = false; //If the email was sent
		_this.email_cache = ''; // Email used to sent previous message
		_this.show_correct = false; //Trigger for correct answer controls
		_this.show_wrong = false; //Trigger for wrong answer controls
		_this.current = { //For storing current states
			question: null, // Current question
			ang_id:  0, // Current question angular id
			//attempt: 1, // Current attempt
			answer: '-1', //Holding current unswer
		};
		_this.user = { // For storing user info
			name: null, // User name
			organisation: null, // User organisation
			email: '', // User E-mail
			score: 0, // User overall score
			attempt: 1, // User overall attempts
		};
		_this.tid = 0; //Loading test id
		if ($attrs.tid) _this.tid = parseInt($attrs.tid.trim());
		if(!Number.isInteger(_this.tid)) _this.tid = 0;

		// DATA RELATED THINGS
		var url = window.location.href.split(/[?#]/)[0];
		$http.post(url, $httpParamSerializerJQLike({
				"ca_quizes_ajax_frontdoor": "true",
				"ca_quizes_ajax_backdoor_tid": _this.tid,//Test ID to load
		}), {
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function(response) {
			_this.questions = response.data[1];
			_this.test_title = response.data[0];
			console.log(_this.tid, _this.questions);
			_this.maxid = _this.questions.length - 1;
			_this.current.question = _this.questions[0];
			console.log(_this.current);
			_this.preloader = false;			
		},
		function(data) {
			alert("Something went wrong! Please, check your internet connection and try to reload the page!");
		});

		//NAVIGATION RELATED
		_this.start_test = function() {
			_this.intro = false;
		}
		_this.answer_confirm = function(val) {
			val = parseInt(val.trim())
			if(_this.current.question.answers[val].correct == true)
				_this.show_correct = true;
			else 
				_this.show_wrong = true;
			console.log(val);
		}
		_this.answer_repeat = function() {
			_this.show_correct = false;
			_this.show_wrong = false;
			_this.current.answer = '-1';
			_this.user.attempt++;
		}
		_this.next_question = function() {
			_this.current.ang_id++;
			if(_this.show_correct) _this.user.score++;
			if(_this.current.ang_id > _this.maxid) {
				_this.current.ang_id--;
				_this.score_screen();
				return;
			}
			_this.show_correct = false;
			_this.show_wrong = false;
			_this.current.answer = '-1';
			_this.current.question = _this.questions[_this.current.ang_id];
		}
		_this.score_screen = function() {
			_this.final = true;
			_this.show_correct = false;
			_this.show_wrong = false;
		}
		_this.ca_quiz_restart_test = function() {
			_this.final = false;
			_this.show_correct = false;
			_this.show_wrong = false;
			_this.current.answer = '-1';
			_this.current.question = _this.questions[0];
			_this.current.ang_id = 0;
			_this.user.attempt = 1;
			_this.intro = true;
			_this.email_sent = false;
			_this.user.score = 0;
		}
		_this.email_results = function() {
			_this.email_sent = false;
			$http.post('', $httpParamSerializerJQLike({
				"ca_quizes_ajax_frontdoor": "true",
				"ca_quizes_ajax_frontdoor_send_email": "true",
				"ca_quizes_ajax_frontdoor_email": _this.user.email,
				"ca_quizes_ajax_frontdoor_test_name": _this.test_title,
				"ca_quizes_ajax_frontdoor_correct": _this.user.score,
				"ca_quizes_ajax_frontdoor_attempts": _this.user.attempt,
			}), {
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function(response) {
				_this.email_cache = _this.user.email;
				_this.email_sent = true;
				console.log(response.data);
			},
			function(data) {
				alert("Something went wrong! Please, check your internet connection and try to reload the page!");
			});
		}
	}
});