angular.
module('ca_quiz').
component('caQuizView', {
	templateUrl: '/wp-content/plugins/ca_quizes/admin_layouts/angular_templates/admin_main_menu_list.html',
	controller: function ca_quiz_ctrl($http, $httpParamSerializerJQLike) {
		_this = this;
		_this.editor_state = false; //Editor popup visibility state
		_this.editor_new_question = false; //Toggles add new question mode
		_this.editor_new_test = false; //Toggles add new test mode
		_this.editor_qid = 0; //Currently edited question
		_this.editor_tid = 0; //Current test
		_this.preloader = true; //Preloader activity
		_this.processing_data = false; //If some data processing currently in action
		_this.ca_editor = {}; //The editor container
		_this.ca_editor.title = null; //Title field of the editor
		_this.ca_editor.answers = null; //Questions textarea of the editor
		// TOGGLE OPEN/CLOSE TEST BLOCKS
		_this.ca_toggle_block_closed = function(tid) {
			cls = _this.tests[tid].closed;
			if(cls == ' ca_block_closed') cls = ''; else cls = ' ca_block_closed';
			_this.tests[tid].closed = cls;
		};
		// EDITOR RELATED STUFF
		_this.ca_close_quiz_editor = function() {
			_this.editor_state = false;
			_this.editor_new_question = false;
			_this.editor_new_test = false;
			_this.ca_editor.title = null;
			_this.ca_editor.answers = null;
			_this.editor_edit_test = false;
			_this.ca_editor.title = null;
			_this.ca_editor.answers = null;
			_this.editor_qid = 0;
			_this.editor_tid = 0;
		}
		_this.ca_open_quiz_editor = function(tid, qid) {
			if(_this.processing_data == true) return false;
			_this.editor_state = true;
			_this.editor_tid = tid;
			_this.editor_qid = qid;
			_this.ca_editor.title = _this.tests[_this.editor_tid].questions[_this.editor_qid].title;
			_this.ca_editor.answers = _this.tests[_this.editor_tid].questions[_this.editor_qid].answers;
		}
		_this.ca_editor_create_question = function(tid) {
			if(_this.processing_data == true) return false;
			_this.editor_state = true;
			_this.editor_tid = tid;
			_this.editor_new_question = true;
			_this.editor_qid = -1;
		}
		_this.ca_editor_create_test = function() {
			if(_this.processing_data == true) return false;
			_this.editor_state = true;
			_this.editor_new_test = true;
			_this.editor_qid = -1;
		}
		_this.ca_editor_edit_test = function(tid) {
			if(_this.processing_data == true) return false;
			_this.editor_state = true;
			_this.editor_edit_test = true;
			_this.ca_editor.title = _this.tests[tid].title;
			_this.editor_qid = -1;
			_this.editor_tid = tid;
		}
		// DATA RELATED THINGS
		$http.get('/wp-admin/admin.php?page=ca_quizes%2Fca_quizes.php&ca_quizes_ajax_backdoor=1').then(function(response) {
			_this.tests = response.data;
			_this.preloader = false;
		},
		function(data) {
			alert("Something went wrong! Please, check your internet connection and try to reload the page!");
		});
		_this.ca_create_new_question = function(tid) {
			console.log("I'm working fine on creation");
			if((_this.ca_editor.title != null) && (_this.ca_editor.title != "")) {
				_this.processing_data = true;
				_this.editor_state = false;
				$http.post('/wp-admin/admin.php?page=ca_quizes%2Fca_quizes.php', $httpParamSerializerJQLike({
						"ca_quizes_ajax_backdoor": "true",
						"ca_quizes_ajax_backdoor_new_question": "true",
						"ca_quizes_ajax_backdoor_tid": _this.tests[tid].id,//Test real ID
						"ca_quizes_ajax_backdoor_count": _this.tests[tid].questions.length + 1,//Put new question to be last in order
						"ca_quizes_ajax_backdoor_title": _this.ca_editor.title,
						"ca_quizes_ajax_backdoor_answers": _this.ca_editor.answers,
				}), {
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function(response) {
					console.log(response.data.trim());
					res = parseInt(response.data.trim());
					if(Number.isInteger(res)) {
						_this.tests[tid].questions.push(
							{
								id: res,
								ang_id: _this.tests[tid].questions.length,
								order_val: _this.tests[tid].questions.length + 1,
								title: _this.ca_editor.title,
								answers: _this.ca_editor.answers,
							}
						);
						console.log(_this.tests[tid].questions);
					}
					else alert("Something went wrong with question creation!");
					_this.processing_data = false;
					_this.ca_close_quiz_editor();

				},
				function(data) {
					alert("Something went wrong! Please, check your internet connection and try to reload the page!");
					_this.processing_data = false;
					_this.ca_close_quiz_editor();
				});
			}
		}
		_this.ca_edit_question = function(tid, qid) {
			console.log("I'm working fine on edits");
			_this.processing_data = true;
			_this.editor_state = false;
			$http.post('/wp-admin/admin.php?page=ca_quizes%2Fca_quizes.php', $httpParamSerializerJQLike({
					"ca_quizes_ajax_backdoor": "true",
					"ca_quizes_ajax_backdoor_edit_question": "true",
					"ca_quizes_ajax_backdoor_tid": _this.tests[tid].id,
					"ca_quizes_ajax_backdoor_qid": _this.tests[tid].questions[qid].id,
					"ca_quizes_ajax_backdoor_title": _this.ca_editor.title,
					"ca_quizes_ajax_backdoor_answers": _this.ca_editor.answers,
			}), {
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function(response) {
				console.log(response.data);
				console.log(response.data.trim());
				if(response.data.trim() == "OK") {
					_this.tests[tid].questions[qid].title =	_this.ca_editor.title;
					_this.tests[tid].questions[qid].answers =	_this.ca_editor.answers;
					_this.processing_data = false;
					_this.ca_close_quiz_editor();
				}
				else alert("Something went wrong with question editing!");
				_this.processing_data = false;
				_this.ca_close_quiz_editor();

			},
			function(data) {
				alert("Something went wrong! Please, check your internet connection and try to reload the page!");
				console.log(data);
				_this.processing_data = false;
				_this.ca_close_quiz_editor();
			});
		}
		_this.ca_move_question = function(up, tid, qid) {
			if(up == undefined) up = false;
			if((qid == 0) && (up == true)) return;
			if((qid == (_this.tests[tid].questions.length - 1)) && (up == false)) return;
			dq = up ? -1 : 1;
			_this.processing_data = true;
			$http.post('/wp-admin/admin.php?page=ca_quizes%2Fca_quizes.php', $httpParamSerializerJQLike({
					"ca_quizes_ajax_backdoor": "true",
					"ca_quizes_ajax_backdoor_move_question": "true",
					"ca_quizes_ajax_backdoor_tid": _this.tests[tid].id,
					"ca_quizes_ajax_backdoor_qid": _this.tests[tid].questions[qid].id,
					"ca_quizes_ajax_backdoor_dq": dq,
			}), {
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function(response) {
				console.log(response.data.trim());
				if(response.data.trim() == "OK") {
					q = _this.tests[tid].questions[qid];
					q.ang_id = qid + dq;
					_this.tests[tid].questions[qid] = _this.tests[tid].questions[qid + dq];
					_this.tests[tid].questions[qid].ang_id = qid;
					_this.tests[tid].questions[qid + dq] = q;
				}
				else alert("Something went wrong with question movement!");
				_this.processing_data = false;
				_this.ca_close_quiz_editor();

			},
			function(data) {
				alert("Something went wrong! Please, check your internet connection and try to reload the page!");
				console.log(data);
				_this.processing_data = false;
				_this.ca_close_quiz_editor();
			});
		}
		_this.ca_delete_question = function(tid, qid) {
			if (confirm('Are you really want to delete this question?')) {
				_this.processing_data = true;
				$http.post('/wp-admin/admin.php?page=ca_quizes%2Fca_quizes.php', $httpParamSerializerJQLike({
						"ca_quizes_ajax_backdoor": "true",
						"ca_quizes_ajax_backdoor_delete_question": "true",
						"ca_quizes_ajax_backdoor_tid": _this.tests[tid].id,
						"ca_quizes_ajax_backdoor_qid": _this.tests[tid].questions[qid].id,
				}), {
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function(response) {
					console.log(response.data.trim());
					if(response.data.trim() == "OK") {
						_this.tests[tid].questions.splice(qid, 1);
						_this.tests[tid].questions.forEach(function(val, num) {
							_this.tests[tid].questions[num].ang_id = num;
						});
						_this.processing_data = false;
						_this.ca_close_quiz_editor();
					}
					else alert("Something went wrong with question deletion!");
					_this.processing_data = false;
					_this.ca_close_quiz_editor();

				},
				function(data) {
					alert("Something went wrong! Please, check your internet connection and try to reload the page!");
					console.log(data);
					_this.processing_data = false;
					_this.ca_close_quiz_editor();
				});
			} else {
				return;
			}
		}
		_this.ca_create_new_test = function() {
			console.log("I'm working fine on test creation");
			_this.processing_data = true;
			_this.editor_state = false;
			$http.post('/wp-admin/admin.php?page=ca_quizes%2Fca_quizes.php', $httpParamSerializerJQLike({
					"ca_quizes_ajax_backdoor": "true",
					"ca_quizes_ajax_backdoor_create_test": "true",
					"ca_quizes_ajax_backdoor_title": _this.ca_editor.title,
			}), {
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function(response) {
				console.log(response.data.trim());
				res = parseInt(response.data.trim());
				if(Number.isInteger(res)) {
					_this.tests.push({
						id: res,
						ang_id: _this.tests.length,
						title: _this.ca_editor.title,
						questions: [],
					});
					_this.processing_data = false;
					_this.ca_close_quiz_editor();
				}
				else alert("Something went wrong with test creation!");
				_this.processing_data = false;
				_this.ca_close_quiz_editor();

			},
			function(data) {
				alert("Something went wrong! Please, check your internet connection and try to reload the page!");
				console.log(data);
				_this.processing_data = false;
				_this.ca_close_quiz_editor();
			});
		}
		_this.ca_edit_test = function(tid) {
			console.log("I'm working fine on test update");
			_this.processing_data = true;
			_this.editor_state = false;
			$http.post('/wp-admin/admin.php?page=ca_quizes%2Fca_quizes.php', $httpParamSerializerJQLike({
					"ca_quizes_ajax_backdoor": "true",
					"ca_quizes_ajax_backdoor_edit_test": "true",
					"ca_quizes_ajax_backdoor_title": _this.ca_editor.title,
					"ca_quizes_ajax_backdoor_tid": _this.tests[tid].id,
			}), {
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function(response) {
				console.log(response.data.trim());
				if(response.data.trim() == 'OK') {
					_this.tests[tid].title = _this.ca_editor.title;
					_this.processing_data = false;
					_this.ca_close_quiz_editor();
				}
				else alert("Something went wrong with test update!");
				_this.processing_data = false;
				_this.ca_close_quiz_editor();

			},
			function(data) {
				alert("Something went wrong! Please, check your internet connection and try to reload the page!");
				console.log(data);
				_this.processing_data = false;
				_this.ca_close_quiz_editor();
			});
		}
		_this.ca_delete_test = function(tid) {
			if (confirm('Are you really want to delete this test and all questions within?')) {
				console.log("I'm working fine on test deliting");
				_this.processing_data = true;
				_this.editor_state = false;
				$http.post('/wp-admin/admin.php?page=ca_quizes%2Fca_quizes.php', $httpParamSerializerJQLike({
						"ca_quizes_ajax_backdoor": "true",
						"ca_quizes_ajax_backdoor_delete_test": "true",
						"ca_quizes_ajax_backdoor_tid": _this.tests[tid].id,
				}), {
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function(response) {
					console.log(response.data.trim());
					if(response.data.trim() == 'OK') {
						_this.tests.splice(tid, 1);
						_this.tests.forEach(function(val, num) {
							_this.tests[num].ang_id = num;
						})
						_this.processing_data = false;
						_this.ca_close_quiz_editor();
					}
					else alert("Something went wrong with test deleting!");
					_this.processing_data = false;
					_this.ca_close_quiz_editor();

				},
				function(data) {
					alert("Something went wrong! Please, check your internet connection and try to reload the page!");
					console.log(data);
					_this.processing_data = false;
					_this.ca_close_quiz_editor();
				});
			}
		}
	}
});