<?php
add_action( 'init', 'ca_quizes_init_shortcodes' );
function ca_quizes_init_shortcodes() {
	add_shortcode( 'ca_quiz', 'ca_quiz_main_shortcode' );
}

function ca_quiz_main_shortcode($atts) {
	wp_enqueue_script( 'ca_quizes_angular' ); // Adding angular
	wp_enqueue_script( 'ca_quizes_angular_sanitize' );
	wp_enqueue_script( 'ca_quizes_angular_module' );
	wp_enqueue_script( 'ca_quizes_angular_components_front_end_output' );
	wp_enqueue_script( 'ca_quizes_angular_filters_html_output' );

	wp_enqueue_style( 'ca_quizes_front');
	$title_trig = true;
	foreach($atts as $att)
		if($att == 'notitle') $title_trig = false;
	$atts = shortcode_atts( array(
		'id' => 0,
	), $atts, 'ca_quiz' );
	$tid = intval($atts['id']);
	$qry = new WP_Query(array(
		'post_type' => 'ca_test',
		'posts_per_page' => -1,
		'post__in' => array($tid),
	));
	ob_start();
	while($qry->have_posts()) : $qry->the_post();
		if($title_trig == true) :
	?>
			<h2><?php the_title(); ?></h2>
	<?php
		endif;
	?>
		<div ng-app="ca_quiz">
			<ca-quiz-front tid="<?php echo $tid; ?>"></ca-quiz-front>
		</div>
		<ul>
	<?php
	endwhile;
	wp_reset_query();
	return ob_get_clean();
}
?>