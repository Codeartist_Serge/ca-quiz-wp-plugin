<?php
	add_action('admin_init', 'ca_quizes_ajax_backdoor');
	function ca_quizes_ajax_backdoor() {
		if($_GET['ca_quizes_ajax_backdoor'] == 1) { // JSON WITH ALL OF THE DATA
			$data = array();
			$qry_tst = new WP_Query(array(
				'post_type' => 'ca_test',
				'posts_per_page' => -1,
				'orderby' => 'id',
				'order' => 'ASC'
			));
			$num = 0;
			while($qry_tst->have_posts()) : $qry_tst->the_post();
				$qry_qst = new WP_Query(array(
					'post_type'  => 'ca_question',
					'posts_per_page' => -1,
					'meta_query' => array(
						array(
							'key'     => '_ca_quizes_test_id',
							'value'   => get_the_ID(),
							'compare' => 'IN',
						),
					),
					'order' => 'ASC',
					'orderby' => 'meta_value_num',
					'meta_key'  => '_ca_quizes_order',
				));
				$questions = array();
				$num1 = 0;
				$tid = get_the_ID();
				$test_title = get_the_title();
				while($qry_qst->have_posts()) : $qry_qst->the_post();
					$questions[] = array(
						'id' => get_the_ID(),
						'ang_id' => $num1,
						'title' => get_the_title(),
						'answers' => get_the_content(),
						'order_val' => get_post_meta(get_the_ID(), '_ca_quizes_order', true),
					);
					$num1++;
				endwhile;
				$data[] = array(
					'id' => $tid,
					'ang_id' => $num,
					'title' => $test_title,
					'closed' => ' ca_block_closed',
					'questions' => $questions,
				);
				$num++;
			endwhile;
			echo json_encode($data);
			die();

		}
		if($_POST['ca_quizes_ajax_backdoor'] == true) { //POST REQUEST FOR CHANGES
			if($_POST['ca_quizes_ajax_backdoor_new_question'] == true) { //ADDING NEW QUESTION
				$tid = $_POST['ca_quizes_ajax_backdoor_tid'];
				$count = $_POST['ca_quizes_ajax_backdoor_count'];
				$title = $_POST['ca_quizes_ajax_backdoor_title'];
				$answers = $_POST['ca_quizes_ajax_backdoor_answers'];
				if($title == null || $title == '') {
					echo'Empty title';
					die();
				}
				$res = wp_insert_post(array(
					'post_title' => wp_strip_all_tags(sanitize_text_field($title), true),
					'post_status' => 'publish',
					'post_type' => 'ca_question',
					'meta_input' => array(
						'_ca_quizes_test_id' => $tid,
					),
					'post_content' => wp_strip_all_tags(implode( "\n", array_map( 'sanitize_text_field', explode( "\n", $answers ) ) ), false),
				));
				if(is_wp_error( $res )) {
					echo'Error creating post';
					die();
				}
				$res1 = update_post_meta($res, '_ca_quizes_test_id', intval($tid));
				if($res1 == false && get_post_meta($res, '_ca_quizes_test_id', true) != intval($tid)) {
					wp_delete_post( $res, true );
					echo'Error creating post metadata for test ID';
					die();
				}
				$res1 = update_post_meta($res, '_ca_quizes_order', intval($count));
				if($res1 == false && get_post_meta($res, '_ca_quizes_order', true) != intval($count)) {
					wp_delete_post( $res, true );
					echo'Error creating post metadata for question order';
					die();
				}
				echo $res;
				die();
			}
			if($_POST['ca_quizes_ajax_backdoor_move_question'] == true) { //MOVING THE QUESTION
				$tid = $_POST['ca_quizes_ajax_backdoor_tid'];
				$qid = $_POST['ca_quizes_ajax_backdoor_qid'];
				$dq = intval($_POST['ca_quizes_ajax_backdoor_dq']);
				$qry_qst = new WP_Query(array(
					'post_type'  => 'ca_question',
					'meta_query' => array(
						array(
							'key'     => '_ca_quizes_test_id',
							'value'   => $tid,
							'compare' => 'IN',
						),
					),
					'order' => 'ASC',
					'orderby' => 'meta_value_num',
					'meta_key'  => '_ca_quizes_order',
				));
				$questions = array();
				$current_id = 0;
				$num = 0;
				while($qry_qst->have_posts()) {
					$qry_qst->the_post();
					$questions[] = array(
						'id' => get_the_ID(),
						'order' => get_post_meta(get_the_ID(), '_ca_quizes_order', true),
					);
					if(get_the_ID() == $qid) $current_id = $num;
					$num++;
				}
				if(($current_id == 0) && (dq == -1)) {
					echo "The first question can't be moved up";
					die();
				}
				if(($current_id == ($num - 1)) && (dq == 1)) {
					echo "The last question can't be moved down";
					die();
				}
				if($num == 0) {
					echo "There are no questions present in this test, create at list two of them first!";
					die();
				}
				$next_id = $current_id + $dq;
				$res = update_post_meta($questions[$current_id]['id'], '_ca_quizes_order', $questions[$next_id]['order']);
				if($res == false) {
					echo'Error updating current post metadata: dq='.$dq.' current_id='.$current_id.' next_id='.$next_id.' ids:'.$questions[$current_id]['id'].'/'.$questions[$next_id]['id'].' orders: '.$questions[$current_id]['order'].'/'.$questions[$next_id]['order'];
					die();
				}
				$res = update_post_meta($questions[$next_id]['id'], '_ca_quizes_order', $questions[$current_id]['order']);
				if($res == false) {
					echo'Error updating neighbor post metadata: dq='.$dq.' current_id='.$current_id.' next_id='.$next_id.' ids:'.$questions[$current_id]['id'].'/'.$questions[$next_id]['id'].' orders: '.$questions[$current_id]['order'].'/'.$questions[$next_id]['order'];
					die();
				}
				echo 'OK';
				die();
			}
			if($_POST['ca_quizes_ajax_backdoor_edit_question'] == true) { //EDITING QUESTION
				$tid = $_POST['ca_quizes_ajax_backdoor_tid'];
				$qid = $_POST['ca_quizes_ajax_backdoor_qid'];
				$title = $_POST['ca_quizes_ajax_backdoor_title'];;
				$answers = $_POST['ca_quizes_ajax_backdoor_answers'];;
				$res = wp_insert_post(array(
					'ID' => $qid,
					'post_title' => wp_strip_all_tags(sanitize_text_field($title), true),
					'post_status' => 'publish',
					'post_type' => 'ca_question',
					'meta_input' => array(
						'_ca_quizes_test_id' => $tid,
					),
					'post_content' => wp_strip_all_tags(implode( "\n", array_map( 'sanitize_text_field', explode( "\n", $answers ) ) ), false),
				));
				if(is_wp_error( $res )) {
					echo'Error creating post';
					die();
				}
				echo 'OK';
				die();
			}
			if($_POST['ca_quizes_ajax_backdoor_delete_question'] == true) { //DELETING QUESTION
				$tid = $_POST['ca_quizes_ajax_backdoor_tid'];
				$qid = $_POST['ca_quizes_ajax_backdoor_qid'];
				if(wp_delete_post( $qid, true ) != false) {
					$qry_qst = new WP_Query(array(
						'post_type'  => 'ca_question',
						'meta_query' => array(
							array(
								'key'     => '_ca_quizes_test_id',
								'value'   => $tid,
								'compare' => 'IN',
							),
						),
						'order' => 'ASC',
						'orderby' => 'meta_value_num',
						'meta_key'  => '_ca_quizes_order',
					));
					$num = 1;
					while($qry_qst->have_posts()) {
						$qry_qst->the_post();
						update_post_meta(get_the_ID(), '_ca_quizes_order', $num);
						$num++;
					}
					echo 'OK';
					die();
				}
				else {
					echo 'Error while deleting post...';
					die();
				}
			}
			if($_POST['ca_quizes_ajax_backdoor_create_test'] == true) { //CREATING TEST
				$title = $_POST['ca_quizes_ajax_backdoor_title'];
				$res = wp_insert_post(array(
					'post_title' => wp_strip_all_tags(sanitize_text_field($title), true),
					'post_status' => 'publish',
					'post_type' => 'ca_test',
				));
				if(is_wp_error( $res )) {
					echo'Error creating post';
					die();
				}
				echo $res;
				die();
			}
			if($_POST['ca_quizes_ajax_backdoor_edit_test'] == true) { //EDIT TEST
				$title = $_POST['ca_quizes_ajax_backdoor_title'];
				$tid = $_POST['ca_quizes_ajax_backdoor_tid'];
				$res = wp_insert_post(array(
					'ID' => $tid,
					'post_title' => wp_strip_all_tags(sanitize_text_field($title), true),
					'post_status' => 'publish',
					'post_type' => 'ca_test',
				));
				if(is_wp_error( $res )) {
					echo'Error updating the post';
					die();
				}
				echo 'OK';
				die();
			}
			if($_POST['ca_quizes_ajax_backdoor_delete_test'] == true) { //DELETE TEST
				$tid = $_POST['ca_quizes_ajax_backdoor_tid'];
				$qry_qst = new WP_Query(array(
					'post_type'  => 'ca_question',
					'meta_query' => array(
						array(
							'key'     => '_ca_quizes_test_id',
							'value'   => $tid,
							'compare' => 'IN',
						),
					),
					'order' => 'ASC',
					'orderby' => 'meta_value_num',
					'meta_key'  => '_ca_quizes_order',
				));
				while($qry_qst->have_posts()) {
					$qry_qst->the_post();
					$res = wp_delete_post(get_the_ID(), true);
					if($res == false) {
						echo'Error deleting a question inside the test';
						die();
					}
				}
				$res = wp_delete_post( $tid, true );
				if($res ==false) {
					echo'Error deleting the post';
					die();
				}
				echo 'OK';
				die();
			}
		}
	}
?>