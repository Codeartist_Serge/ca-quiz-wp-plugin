<?php
	add_action( 'init', 'ca_quizes_pdf_generator' );
	function ca_quizes_pdf_generator() {
		if($_POST['ca_quiz_pdf_generator'] == true) {
	//==================================================================START PDF GENERATION===========================================================
			$test_name = $_POST['ca_quiz_pdf_test_name'];
			$user_name = $_POST['ca_quiz_pdf_user_name'];
			$user_workplace = $_POST['ca_quiz_pdf_user_workplace'];
			$correct = $_POST['ca_quiz_pdf_correct'];
			$attempts = $_POST['ca_quiz_pdf_attempts'];
			require_once('fpdf/fpdf.php');
			//create a FPDF object
			$pdf=new FPDF();
			//set document properties
			$pdf->SetAuthor('http://caretrainingonline.com');
			$pdf->SetTitle('Quiz results');
			//set font for the entire document
			$pdf->SetFont('Helvetica','B',9);
			$pdf->SetTextColor(54,95,144);
			$pdf->SetLeftMargin(62);
			$pdf->SetRightMargin(62);
			$pdf->SetTopMargin(30);
			//set up a page
			$pdf->AddPage('P');
			$pdf->SetDisplayMode(real,'default');
			$pdf->SetFontSize(24);
			$pdf->MultiCell( 0, 8, iconv('UTF-8', 'windows-1252', 'Congratulations!'), 0, 'C');
			$pdf->Ln(1);
			$pdf->SetFont('Helvetica','I',13);
			$pdf->SetTextColor(50,60,100);
			$pdf->MultiCell( 0, 10, iconv('UTF-8', 'windows-1252', 'You have completed'), 0, 'C');
			$pdf->Ln(0.7);
			$pdf->SetFont('Helvetica','B',14);
			$pdf->MultiCell( 0, 10, iconv('UTF-8', 'windows-1252', $test_name), 0, 'C');
			$pdf->Ln(1.5);
			$pdf->SetFont('Helvetica','',13);
			$pdf->Write(10,'Name:');
			$pdf->setX($pdf->getX() + 12.5);
			$pdf->SetFont('Helvetica','B',13);
			$pdf->Write(10, iconv('UTF-8', 'windows-1252', $user_name));
			$pdf->Ln(7);
			$pdf->SetFont('Helvetica','',13);
			$pdf->Write(10,'Workplace:');
			$pdf->setX($pdf->getX() + 3);
			$pdf->SetFont('Helvetica','B',13);
			$pdf->Write(10, iconv('UTF-8', 'windows-1252', $user_workplace));
			$pdf->Ln(17);
			$pdf->SetFont('Helvetica','',13);
			$pdf->Write(10,'Your Result:');
			$pdf->Ln(7);
			$pdf->SetFont('Helvetica','B',13);
			$pdf->Write(10, iconv('UTF-8', 'windows-1252', $correct).' Correct Answers with '.iconv('UTF-8', 'windows-1252', $attempts).' Attempts');
			$pdf->Ln(17);
			$pdf->SetTextColor(20,53,253);
			$pdf->SetFont('Helvetica','BIU',13);
			$pdf->Cell(0, 10, 'www.CareTrainingOnline.com', 0, 1, 'C', false, 'www.CareTrainingOnline.com');
			//Output the document
			header("Content-Type: application/pdf");
			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header('Content-Disposition: attachment; filename="example1.pdf"');
			header("Content-Transfer-Encoding: binary ");
			$pdf->Output('example1.pdf','I');
			
	//==================================================================END PDF GENERATION===========================================================
			die();
		}
	}
?>