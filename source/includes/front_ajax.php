<?php
	add_action('init', 'ca_quizes_ajax_frontdoor');
	function ca_quizes_ajax_frontdoor() {
		if($_POST['ca_quizes_ajax_frontdoor'] == true) {
			if($_POST['ca_quizes_ajax_frontdoor_send_email'] == true) {
				$mail = sanitize_email($_POST['ca_quizes_ajax_frontdoor_email']);
				$test_name = strip_tags($_POST['ca_quizes_ajax_frontdoor_test_name']);
				$correct = strip_tags($_POST['ca_quizes_ajax_frontdoor_correct']);
				$attempts = strip_tags($_POST['ca_quizes_ajax_frontdoor_attempts']);
				$message = '<h3>Congratulations! You have completed «'.$test_name.'»</h3>'."\n".
					'<p>Name: Some name<br>'."\n".
					'Workplace: Some work</p>'."\n".
					'<h3>Your Result:<br>'."\n".
					$correct.' Correct Answers with '.$attempts.' Attempts</h3>';
				$currentDomain = preg_replace('/www\./i', '', $_SERVER['SERVER_NAME']);
				$headers[] = 'Content-Type: text/html; charset=UTF-8';
				$headers[] = 'From: Care Training Online <no-reply@'.$currentDomain.'>';
				$res = wp_mail( $mail, "Test results", $message, $headers);
				die();
			}
			else {
				$tid = intval($_POST['ca_quizes_ajax_backdoor_tid']);
				$qry = new WP_Query(array(
					'post_type' => 'ca_test',
					'posts_per_page' => 1,
					'post__in' => array($tid),
				));
				while($qry->have_posts()) { $qry->the_post(); $test_title = get_the_title(); }
				$qry = new WP_Query(array(
					'post_type'  => 'ca_question',
					'meta_query' => array(
						array(
							'key'     => '_ca_quizes_test_id',
							'value'   => $tid,
							'compare' => 'IN',
						),
					),
					'order' => 'ASC',
					'orderby' => 'meta_value_num',
					'meta_key'  => '_ca_quizes_order',
				));
				$questions = array();
				$num1 = 0;
				while($qry->have_posts()) : $qry->the_post();
					$answers = array();
					$ans = explode("\n", get_the_content());
					$num = 0;
					$cor_trg = true;
					foreach($ans as $an) {
						$cor = substr(trim($an), 0, 1) == '*' ? true : false;
						if($cor == true) $cor_trg = false;
						$answers[] = array(
							'name' => substr(trim($an), 0, 1) == '*' ? substr(trim($an), 1) : trim($an),
							'correct' => $cor,
							'ang_id' => $num,
						);
						$num++;
					}
					if($cor_trg == true) $answers[0]['correct'] = true; //If there were no * mark, lets assume that the first answer is correct
					$questions[] = array(
						'id' => get_the_ID(),
						'ang_id' => $num1,
						'title' => get_the_title(),
						'answers' => $answers,
						'order_val' => get_post_meta(get_the_ID(), '_ca_quizes_order', true),
					);
					$num1++;
				endwhile;
				$res = array(
					$test_title,
					$questions,
				);
				echo json_encode($res);
				die();
			}
		}
	}
?>