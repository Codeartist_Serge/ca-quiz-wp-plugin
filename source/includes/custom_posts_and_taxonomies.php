<?php
function ca_quizes_init_custom_posts() {
	$args = array(
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => false,
		'show_in_nav_menus' => false,
		'show_in_menu' => false,
		'show_in_admin_bar' => false,

		'label' => 'CA Tests',
		'supports' => array( 'title' ),
	);
	register_post_type( 'ca_test', $args );
	$args = array(
		'public' => true,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => false,
		'show_in_nav_menus' => false,
		'show_in_menu' => false,
		'show_in_admin_bar' => false,

		'label' => 'CA Questions',
		'supports' => array( 'title', 'editor' ),
	);
	register_post_type( 'ca_question', $args );
}
add_action( 'init', 'ca_quizes_init_custom_posts' );
?>