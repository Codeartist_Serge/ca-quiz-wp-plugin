<h2>CA Quizes options screen</h2>
<div class="metabox-holder ca_quiz_container">
	<div class="postbox-container">
		<div class="meta-box">
			<div class="postbox ">
				<h3 class="hndle"><span>Options</span></h3>
				<div class="inside" ng-app="ca_quiz">
<?php
	if($_POST['ca_quizes_recalculate'] == true) {
		$qry_tst = new WP_Query(array(
			'post_type' => 'ca_test',
			'posts_per_page' => -1,
		));
		$res = false;
		while($qry_tst->have_posts()) {
			$qry_tst->the_post();
			$qry_qst = new WP_Query(array(
				'post_type'  => 'ca_question',
				'meta_query' => array(
					array(
						'key'     => '_ca_quizes_test_id',
						'value'   => get_the_ID(),
						'compare' => 'IN',
					),
				),
				'order' => 'ASC',
				'orderby' => 'meta_value_num',
				'meta_key'  => '_ca_quizes_order',
			));
			$num = 1;
			while($qry_qst->have_posts()) {
				$qry_qst->the_post();
				delete_post_meta(get_the_ID(), '_ca_quizes_order');
				$upd = update_post_meta(get_the_ID(), '_ca_quizes_order', $num);
				if($upd == false) {
					$res = true;
				}
				$num++;
			}
		}
		if($res == true) :
?>
		<p style="color: red !important">Something went wrong with recalculation, please try again!</p>
<?php
		else :
?>
		<p style="color: green !important">Recalculation completed!</p>
<?php
		endif;
	}
?>
					<p>Use option below if you are experiencing some errors managing questions</p>
					<form action="" method="post">
						<input type="hidden" name="ca_quizes_recalculate" value="true">
						<input type="submit" value="Recalculate indexes & question orders">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>