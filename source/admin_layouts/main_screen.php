<h2>CA Quizes dashboard</h2>
<div class="metabox-holder ca_quiz_container">
	<div class="postbox-container">
		<div class="meta-box">
			<div class="postbox ">
				<h3 class="hndle"><span>All tests</span></h3>
				<div class="inside" ng-app="ca_quiz">
					<ca-quiz-view></ca-quiz-view>
				</div>
			</div>
		</div>
	</div>
</div>