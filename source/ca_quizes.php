<?php
/*
Plugin Name: CA Quizes
Plugin URI: http://en.codeartist.ru
Description: Provide an easy interface to create & publish simple quizes for your users
Version: 1.0.0
Author: Sergey Degtyarev
Author URI: http://en.codeartist.ru
*/
add_action('init', 'ca_quizes_register_scripts');
add_action('admin_menu', 'ca_quizes_admin');
add_action('admin_enqueue_scripts', 'ca_quizes_admin_scripts_n_styles' );

function ca_quizes_register_scripts() {
	wp_register_script( 'ca_quizes_angular', plugins_url( 'js/angular/angular.min.js', __FILE__ ), array(), '1.5.9', false );
	wp_register_script( 'ca_quizes_angular_sanitize', plugins_url( 'js/angular/angular-sanitize.min.js', __FILE__ ), array('ca_quizes_angular'), '1.0.0', false );
	wp_register_script( 'ca_quizes_angular_module', plugins_url( 'js/angular/modules/main.js', __FILE__ ), array('ca_quizes_angular_sanitize'), '1.0.0', false );
	wp_register_script( 'ca_quizes_angular_components_front_end_output', plugins_url( 'js/angular/components/front_end_output.js', __FILE__ ), array('ca_quizes_angular_module'), '1.0.0', false );
	wp_register_script( 'ca_quizes_angular_filters_html_output', plugins_url( 'js/angular/filters/html_output.js', __FILE__ ), array('ca_quizes_angular_module'), '1.0.0', false );

	wp_register_style( 'ca_quizes_front', plugins_url( 'css/front_main.css', __FILE__ ), false, '1.0.0', false );
}

function ca_quizes_admin_scripts_n_styles() {
	wp_enqueue_style( 'ca_quizes', plugins_url( 'css/admin_main.css', __FILE__ ), false, '1.0.0', false );
	wp_enqueue_script( 'ca_quizes_angular' );
	wp_enqueue_script( 'ca_quizes_angular_sanitize' );
	wp_enqueue_script( 'ca_quizes_angular_module' );
	wp_enqueue_script( 'ca_quizes_angular_filters_html_output' );
	wp_enqueue_script( 'ca_quizes_angular_directives_trusted_html', plugins_url( 'js/angular/directives/trustedhtml.js', __FILE__ ), array('ca_quizes_angular_module'), '1.0.0', false );
	wp_enqueue_script( 'ca_quizes_angular_components_admin_menu_list', plugins_url( 'js/angular/components/admin_menu_list.js', __FILE__ ), array('ca_quizes_angular_module'), '1.0.0', false );
}

function ca_quizes_admin() {
	add_menu_page("CA Quizes", "CA Quizes", "manage_options", 'ca_quizes/ca_quizes.php', "ca_quizes_admin_main", "", "26.11111111");
	add_submenu_page( 'ca_quizes/ca_quizes.php', "Quiz management", 'Quiz management', 'manage_options', 'ca_quizes/ca_quizes.php', 'ca_quizes_admin_main' );
	add_submenu_page( 'ca_quizes/ca_quizes.php', "Options", 'Options', 'manage_options', 'ca_quizes_sub', 'ca_quizes_admin_sub' );
}

function ca_quizes_admin_main()
{
	require_once('admin_layouts/main_screen.php');
}

function ca_quizes_admin_sub()
{
	require_once('admin_layouts/secondary_screen.php');
}

require_once('includes/admin_ajax.php');
require_once('includes/front_ajax.php');
require_once('includes/custom_posts_and_taxonomies.php');
require_once('includes/shortcodes.php');
require_once('includes/pdf_generator.php');

?>